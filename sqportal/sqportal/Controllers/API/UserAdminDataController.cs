﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using sqportal.DAL;
using sqportal.DAL.Models;
using sqportal.Models.Models;
using sqportal.ServiceLayer;

namespace sqportal.Controllers.API
{
    public class UserAdminDataController : ApiController
    {
        private readonly AccountService _accountService;

        public UserAdminDataController()
        {
            _accountService = new AccountService(new DataRepository());
        }

        [HttpGet]
        public HttpResponseMessage UsersList()
        {
            try
            {
                var result = new PersonAccountListModel
                {
                    Persons = _accountService.GetPersonAccounts(),
                    Roles = _accountService.GetRoles()
                };
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        public HttpResponseMessage UserListByRole(string roleId)
        {
            try
            {
                var result = new PersonAccountListModel
                {
                    Persons = _accountService.GetPersonAccountsByRoles(roleId),
                    Roles = _accountService.GetRoles()
                };
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
        
        [HttpGet]
        public EditPersonAccountModel UserEdit(int id)
        {
            EditPersonAccountModel model;
            try
            {
                model = _accountService.GetEditPersonAccount(id);
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }
        
        [HttpPost]
        public HttpResponseMessage UserEdit(EditPersonAccountModel model)
        {
            try
            {
                _accountService.SavePersonAccount(model);
                //return new HttpResponseMessage(HttpStatusCode.OK);
                var response = Request.CreateResponse(HttpStatusCode.Moved);
                response.Headers.Location = new Uri("http://localhost:13922/Test/UserList");
                return response;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.NotModified);
            }
        }

        [HttpPost]
        public HttpResponseMessage UserCreate(CreateUserViewModel model)
        {
            try
            {
                _accountService.SavePersonAccount(model);
                //return new HttpResponseMessage(HttpStatusCode.OK);
                var response = Request.CreateResponse(HttpStatusCode.Moved);
                response.Headers.Location = new Uri("http://localhost:13922/Test/UserList");
                return response;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.NotModified);
            }
        }
      
        [HttpPost]
        public HttpResponseMessage UserDelete(int id)
        {
            try
            {
                _accountService.DeletePersonAccount(id);
               // return Request.CreateResponse(HttpStatusCode.OK);
                var response = Request.CreateResponse(HttpStatusCode.Moved);
                response.Headers.Location = new Uri("http://localhost:13922/Test/UserList");
                return response;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.NotModified);
                
            }
        }
    }
}

