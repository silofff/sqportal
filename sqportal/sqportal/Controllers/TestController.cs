﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using sqportal.DAL;
using sqportal.Models;
using sqportal.Models.Models;
using sqportal.ServiceLayer;

namespace sqportal.Controllers
{
    [Authorize(Roles = "admin")]
    public class TestController : Controller
    {
        private readonly AccountService _accountService;

         public TestController()
        {
            _accountService = new AccountService(new DataRepository());
        }

        // GET: Test
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UserEdit(int id)
        {
            var acc = _accountService.GetEditPersonAccount(id);
            return View(acc);
        }

        
        public ActionResult CreateUser()
        {
            var model = new CreateUserViewModel();
            var allRoles = HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>().Roles;

            var checkBoxListRole = allRoles.Select(role => new RoleModel()
            {
                Name = role.Name,
                Selected = false,
                ID = role.Id
            }).ToList();

            model.Roles = checkBoxListRole;
            return View(model);
        }

        public ActionResult UserList()
        {
            return View(_accountService.GetPersonAccounts());
        }
    }
}