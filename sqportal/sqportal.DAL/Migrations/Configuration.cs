using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using sqportal.DAL.Models;

namespace sqportal.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<sqportal.DAL.AppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(sqportal.DAL.AppContext context)
        {
        }
    }
}
