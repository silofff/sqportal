﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using sqportal.DAL.Models;
using sqportal.Models.Models;

namespace sqportal.DAL
{
    public class DataRepository
    {
        public string ConnectionString { get; set; }

        private AppContext GetDbContext()
        {
            AppContext db = null;

            db = string.IsNullOrEmpty(ConnectionString)
                ? new AppContext()
                : new AppContext(ConnectionString);
            return db;
        }

        public IEnumerable<string> GetUserRolesByEmail(string email)
        {
            var roleList = new List<string>() {"undefined role"};
            using (var db = GetDbContext())
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                var userId = userManager.FindByEmail(email).Id;

                if (!string.IsNullOrEmpty(userId))
                    roleList = userManager.GetRoles(userId).ToList();
            }
            return roleList;
        }

        public IEnumerable<string> GetUserRolesById(string id)
        {
            var roleList = new List<string>() {"undefined role"};
            using (var db = GetDbContext())
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                if (!string.IsNullOrEmpty(id))
                    roleList = userManager.GetRoles(id).ToList();
            }
            return roleList;
        }

        public List<RoleModel> GetRoleModelListById(string id)
        {
            List<RoleModel> roleModelList;
            using (var db = GetDbContext())
            {
                var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(db));
                var roles = roleManager.Roles.Where(x => x.Users.Any(u => u.UserId == id)).Select(x => x);
                roleModelList = roles.Select(role => new RoleModel()
                {
                    ID = role.Id,
                    Name = role.Name,
                    Selected = true
                }).ToList();
            }
            return roleModelList;
        }

        public UserViewModel GetUserView(string email)
        {
            var roleList = new List<string>() {"undefined role"};

            roleList = GetUserRolesByEmail(email).ToList();

            return new UserViewModel()
            {
                Email = email,
                RoleList = roleList
            };
        }


        public void SavePerson(CreateUserViewModel model)
        {
            using (var db = GetDbContext())
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                var user = new ApplicationUser {UserName = model.Email, Email = model.Email};
                var result = userManager.Create(user, model.Password);
                if (result.Succeeded)
                {
                    var roleList = model.Roles.Where(x => x.Selected && x.Name != null).Select(r => r.Name).ToList();
                    foreach (var role in roleList)
                    {
                        userManager.AddToRole(user.Id, role);
                    }

                    db.People.Add(new Person()
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        User = user
                    });
                    db.SaveChanges();
                }
            }
        }

        public List<PersonAccountModel> GetPersonAccounts()
        {
            var personAccountModelList = new List<PersonAccountModel>();
            using (var db = GetDbContext())
            {
                var peoplesId = db.People.Select(x => x.ID).ToList();

                foreach (var id in peoplesId)
                {
                    personAccountModelList.Add(GetPersonAccount(id));
                }
            }
            return personAccountModelList;
        }

        public List<RoleModel> GetRoles()
        {
            List<RoleModel> roleModelList;
            using (var db = GetDbContext())
            {
                var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(db));
                roleModelList = roleManager.Roles.Select(role => new RoleModel()
                {
                    ID = role.Id,
                    Name = role.Name
                }).ToList();
            }
            return roleModelList;
        }

        public PersonAccountModel GetPersonAccount(int id)
        {
            PersonAccountModel model;
            using (var db = GetDbContext())
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                var people = db.People.Find(id);
                var user = userManager.FindById(people.IdentityUserID);
                var roleModelList = GetRoleModelListById(user.Id);

                model = new PersonAccountModel()
                {
                    ID = people.ID,
                    Email = user.Email,
                    FirstName = people.FirstName,
                    LastName = people.LastName,
                    UserIdentityID = people.IdentityUserID,
                    Roles = roleModelList
                };
            }
            return model;
        }

        public void SavePersonAccount(EditPersonAccountModel model)
        {
            using (var db = GetDbContext())
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var account = model.PersonAccount;
                        account.Roles = GetRoleModelListById(account.UserIdentityID);

                        var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

                        var user = userManager.FindById(account.UserIdentityID);
                        user.UserName = account.Email;
                        user.Email = account.Email;
                        userManager.Update(user);

                        var person = new Person
                        {
                            ID = account.ID,
                            FirstName = account.FirstName,
                            LastName = account.LastName,
                            IdentityUserID = account.UserIdentityID,
                            User = user
                        };

                       // UpdatePerson(person);
                        db.Entry(person).State = EntityState.Modified;
                        UpdateRoleMembershipInContext(userManager, account, model.Roles);


                        db.SaveChanges();

                        dbTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback();
                        throw new InvalidOperationException("Save user error.", ex);
                    }
                }
            }
        }

        private void UpdatePerson(Person person)
        {
            using (var db = GetDbContext())
            {
             
            }
        }

        private void UpdateRoleMembershipInContext(UserManager<ApplicationUser> userManager, PersonAccountModel account, List<RoleModel> roles)
        {
            //if roles were added, add them
            foreach (var role in roles.Where(x => x.Selected == true))
            {
                if (!account.Roles.Exists(x => x.ID.Equals(role.ID)))
                {
                    userManager.AddToRole(account.UserIdentityID, role.Name);
                }
            }

            //if roles were deleted, delete them
            foreach (var role in account.Roles)
            {
                if (roles.Exists(x => x.ID.Equals(role.ID) && x.Selected == false))
                {
                    userManager.RemoveFromRole(account.UserIdentityID, role.Name);
                }
            }
        }

        public void DeletePersonAccount(int personId)
        {
            using (var db = GetDbContext())
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var person = db.People.FirstOrDefault(x => x.ID == personId);
                        if (person != null)
                        {
                            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                            var user = userManager.FindById(person.IdentityUserID);
                            userManager.Delete(user);

                            db.SaveChanges();
                            dbTransaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback();
                        throw new InvalidOperationException(String.Format("Delete user error. User ID: {0}", personId), ex);
                    }
                }
            }
        }

        public List<PersonAccountModel> GetPersonAccountsByRoles(string roleId)
        {
            var personAccountModelList = new List<PersonAccountModel>();
            using (var db = GetDbContext())
            {
                var peoplesId = db.People.Where(x => x.User.Roles.Select(y => y.RoleId).Contains(roleId)).Select(p => p.ID);

                foreach (var id in peoplesId)
                {
                    personAccountModelList.Add(GetPersonAccount(id));
                }
            }
            return personAccountModelList;
        }
    }
}
