﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using sqportal.DAL.Models;

namespace sqportal.DAL
{
    public class AppContext : IdentityDbContext<ApplicationUser>
    {
        public AppContext() : base("IdentityDb") { }

        public AppContext(string connectionString)
            : base(connectionString)
        {
        }

        public static AppContext Create()
        {
            return new AppContext();
        }

        public DbSet<Person> People { get; set; }

        static AppContext()
        {
            Database.SetInitializer<AppContext>(new MyContextInitializer());
        }
    }
    class MyContextInitializer : DropCreateDatabaseAlways<AppContext>
    {

        protected override void Seed(AppContext db)
        {
            //add roles
            var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(db));
            roleManager.Create(new ApplicationRole { Name = "admin" });
            roleManager.Create(new ApplicationRole { Name = "user" });
            //add user admin
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var user = new ApplicationUser { UserName = "Admin", Email = "admin@gmail.com" };
            var result = userManager.Create(user, "Test1234");
            if (result.Succeeded)
            {
                userManager.AddToRole(user.Id, "admin");
            }
            //add user admin account
            db.People.Add(new Person()
            {
                FirstName = "Admin",
                LastName = "Adminovich",
                User = user
            });
            db.SaveChanges();
        }
    }


}
