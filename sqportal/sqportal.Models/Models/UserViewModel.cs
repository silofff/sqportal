﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sqportal.Models.Models
{
    public class UserViewModel
    {
        public string Email { get; set; }
        public List<string> RoleList { get; set; }
    }
}
