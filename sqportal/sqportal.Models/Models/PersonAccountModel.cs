﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sqportal.Models.Models
{
    public class PersonAccountModel
    {
        public int ID { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        public String Email { get; set; }
        public string UserIdentityID { get; set; }
        public List<RoleModel> Roles { get; set; }
        public PersonAccountModel()
        {
            Roles = new List<RoleModel>();
        }
    }
}
