﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sqportal.Models.Models
{
    public class RoleModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
}
