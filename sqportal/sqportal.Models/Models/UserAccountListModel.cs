﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sqportal.Models.Models
{
    public class PersonAccountListModel
    {
        public List<PersonAccountModel> Persons { get; set; }
        public List<RoleModel> Roles { get; set; }
    }
}
