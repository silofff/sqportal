﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sqportal.Models.Models;

namespace sqportal.Models.Models
{
    public class EditPersonAccountModel
    {
        public PersonAccountModel PersonAccount { get; set; }
        public List<RoleModel> Roles { get; set; }
    }
}
