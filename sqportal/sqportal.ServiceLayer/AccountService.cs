﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sqportal.DAL;
using sqportal.DAL.Models;
using sqportal.Models.Models;


namespace sqportal.ServiceLayer
{
    public class AccountService
    {
        private readonly DataRepository _repository;

        public AccountService(DataRepository repository)
        {
            _repository = repository;
        }
        public UserViewModel GetUserView(string email)
        {
            return _repository.GetUserView(email);
        }

        public List<PersonAccountModel> GetPersonAccounts()
        {
            return _repository.GetPersonAccounts();
        }

        public List<RoleModel> GetRoles()
        {
            return _repository.GetRoles();
        }

        public EditPersonAccountModel GetEditPersonAccount(int id)
        {
            var editPerson = new EditPersonAccountModel
            {
                PersonAccount = _repository.GetPersonAccount(id),
                Roles = _repository.GetRoles(),
            };
            foreach (var role in editPerson.PersonAccount.Roles.Where(x=>x.Selected))
            {
                var r = editPerson.Roles.FirstOrDefault(x => x.ID == role.ID);
                r.Selected = true;
            }
            return editPerson;
        }

        public void SavePersonAccount(EditPersonAccountModel model)
        {
            _repository.SavePersonAccount(model);
        }

        public void DeletePersonAccount(int personId)
        {
            _repository.DeletePersonAccount(personId);
        }

        public void SavePersonAccount(CreateUserViewModel model)
        {
            _repository.SavePerson(model);
        }

        public List<PersonAccountModel> GetPersonAccountsByRoles(string roleId)
        {
            return _repository.GetPersonAccountsByRoles(roleId);
        }
    }
}
